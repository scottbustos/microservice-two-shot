from django.shortcuts import render
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "href",
        "closet_name",
    ]
class ShoeEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id = None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = shoes.objects.filter(bin = bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["bin"]
            bins = BinVO.objects.get(href=href)
            content["bin"] = bins
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False,
            )

@require_http_methods(["GET","DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(shoes, encoder=ShoeEncoder, safe=False)
    else:
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
