import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function ShoeList() {
    const [shoes, setShoes] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok){
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
    useEffect(() => {
        getData();
    }, [])

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Bins</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>{shoe.bin.closet_name}</td>
                <td>{shoe.manufacturer }</td>
                <td>{shoe.model}</td>
                <td>{shoe.color}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}
export default ShoeList;
