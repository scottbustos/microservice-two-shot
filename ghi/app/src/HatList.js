import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function HatList() {
    const [hats, setHats] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok){
            const data = await response.json();
            setHats(data.hats);
        }
    }
    useEffect(() => {
        getData();
    }, [])

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Location</th>
            <th>Style</th>
            <th>Fabric</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{hat.location.closet_name}</td>
                <td>{hat.style_name }</td>
                <td>{hat.fabric}</td>
                <td>{hat.color}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}
export default HatList;
