import React, { useEffect, useState } from 'react';
import {useNavigate} from 'react-router-dom';

function ShoeForm() {
  const navigate = useNavigate()
  const [bins, setBins] = useState([])
  const [formData, setFormData] = useState({
      manufacturer: '',
      model: '',
      color: '',
      bin: '',
  })

  const fetchData = async () => {
      const url = 'http://localhost:8100/api/bins/'
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          setBins(data.bins)
      }
  }
  useEffect(() => {
      fetchData()
  }, [])

  const handleSubmit = async (events) => {
      events.preventDefault()
      const shoeUrl = 'http://localhost:8080/api/shoes/'
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers:{
              'Content-Type': 'application/json',
          }
      }

      const response = await fetch(shoeUrl, fetchConfig)
      if (response.ok) {
        navigate('/shoes')
          }
      }

  const handleFormChange = (e) => {
      const value = e.target.value
      const inputName = e.target.name

      setFormData({
          ...formData,
          [inputName]: value
      })
  }
  return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.model} placeholder="model" required type="text" name="model" id="model" className="form-control" />
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                  <option value="bin">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

export default ShoeForm;
