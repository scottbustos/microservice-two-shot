# Wardrobify

Team:

* Person 1 - Scott Bustos - Shoes
* Person 2 - Sarah Floyd - Hats

## Design
Our team deicided to use bootstrap for our design.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
Scott -
I took the parameters I was given and created the model for shoes. I know I needed to access the model for wardobes bins and so I created a VO model to poll data from Wardorbes into my microservice. Everytime there was a creation for bins in Wardorbes I relied on the poller to retrieve the data for my VO Model.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
I created a hats model with the fields stated in Learn. I used a foreign key to connect with the Wardrobe model. I made a Location Value Object to pull information from the Wardrobe model.
In my view function, I created two encoders to aid with the serialization process in insomnia. The list hat function is made to show the list of hats in the database based on the information from the model, value object and encoder. The show hat function shows the details of a specifc hat, such as its location. It can also be deleted in Insomnia with this function. 
