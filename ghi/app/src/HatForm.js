import React, { useEffect, useState } from "react"
import {useNavigate} from 'react-router-dom';

function HatForm() {
    const navigate = useNavigate()
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        style_name: '',
        fabric: '',
        color: '',
        location: '',
    })

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (events) => {
        events.preventDefault()
        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
          navigate('/hats')
            }
        }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.style_name} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" />
                  <label htmlFor="style_name">Style name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.closet_name}>{location.shelf_number}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

export default HatForm;
