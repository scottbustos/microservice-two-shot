import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';
import HatList from './HatList';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* <Route path="/" element={<ShoeForm />} /> */}
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="Shoes">
            <Route index element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
