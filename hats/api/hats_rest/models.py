from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    href = models.CharField(max_length=200, null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )
    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.style_name
