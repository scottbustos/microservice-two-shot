from django.db import models
from django.urls import reverse
# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    href = models.CharField(max_length=100, unique=True)
class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.manufacturer

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
